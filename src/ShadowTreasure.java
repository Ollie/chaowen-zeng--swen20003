import bagel.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
/**
 * an example Bagel game.
 */
public class ShadowTreasure extends AbstractGame {
    /**
     * background image
     */
    private final Image BACKGROUND = new Image("res/images/background.png");
    /**
     * distance threshold.
     * When the distance between the player and the stationery entity is less than this value,
     * it is judged as meeting
     */
    public static final int CLOSENESS = 50;
    /**
     * the window state is updated only when the number of frames is a multiple of 10
     */
    private final int maxCount = 10;
    /**
     * counter from 1 to maxCount
     */
    private int counter;
    /**
     * the player entity of the game
     */
    private Player player;
    /**
     * the sandwich entity of the game
     */
    private Sandwich sandwich;
    /**
     * the zombie entity of the game
     */
    private Zombie zombie;
    /**
     * end of game indicator
     */
    private boolean isEndGame;

    /**
     * Get zombie object in the game
     * @return zombie object
     */
    public Zombie getZombie() {
        return this.zombie;
    }

    /**
     * Set game state (end or not)
     * @param endOfGame game state
     */
    public void setEndGame(boolean endOfGame) {
        this.isEndGame = endOfGame;
    }
    /**
     * Get sandwich object in the game
     * @return sandwich object
     */
    public Sandwich getSandwich() {
        return sandwich;
    }

    /**
     * constructor
     * @throws IOException
     */
    public ShadowTreasure() throws IOException {
        //load the initial position of entities
        this.readInitialPos("res/IO/environment.csv");
        //Set the counter to one
        this.counter = 1;
        this.isEndGame = false;
        //Print player information
        System.out.println(player.getPos().x + "," + player.getPos().y + "," + player.getEnergy());
    }

    /**
     * Load from input file
     */
    private void readInitialPos(String filename){
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(",");
                //At this time, because of the problem of text coding, it can not be directly compared with the literal amount of string
                String type = parts[0];
                // remove special characters dnd code according to Java
                type = type.replaceAll("[^a-zA-Z0-9]", "");
                int x = Integer.parseInt(parts[1]);
                int y = Integer.parseInt(parts[2]);
                if(type.equals("Player")) {
                    this.player = new Player(x, y, Integer.parseInt(parts[3]));
                }
                else if(type.equals("Zombie")) {
                    this.zombie = new Zombie(x, y);
                }
                else if(type.equals("Sandwich")) {
                    this.sandwich = new Sandwich(x, y);
                }
                else {
                    throw new BagelError("Unknown type: " + type);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Performs a state update.
     */
    @Override
    public void update(Input input)
    {
        //game over
        if (this.isEndGame || input.wasPressed(Keys.ESCAPE))
        {
            Window.close();
        }
        else
        {
            // Draw background
            BACKGROUND.drawFromTopLeft(0, 0);
            // Update status when the TICK_CYCLE is up
            if (counter > maxCount) {
                // update player status
                player.update(this);
                counter = 1;
                System.out.println(String.format("%.2f",player.getPos().x) + "," + String.format("%.2f",player.getPos().y) + "," + player.getEnergy());
            }
            counter++;
            sandwich.draw();
            zombie.draw();
            player.draw();
        }
    }
    /**
     * The entry point for the program.
     */
    public static void main(String[] args) throws IOException {
        ShadowTreasure game = new ShadowTreasure();
        game.run();
    }
}
