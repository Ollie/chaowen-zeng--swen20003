import bagel.Image;
import bagel.util.Point;
/**
 *  a stationary entity: sandwich
 */
public class Sandwich{
    /**
     * the sandwich's image displayed in the window
     */
    private Image image;
    /**
     * a Point object representing the position of the sandwich
     */
    private Point pos;
    /**
     * if display sandwiches in the window
     */
    private boolean visible;
    /**
     * constructor
     * @param x abscissa
     * @param y ordinate
     */
    public Sandwich(double x, double y){
        this.image = new Image("res/images/sandwich.png");
        this.pos = new Point(x,y);
        this.visible = true;
    }
    /**
     * judge if display sandwiches in the window
     * @return visible
     */
    public boolean isVisible() {
        return visible;
    }
    /**
     * @return the Point object representing the position of the sandwich
     */
    public Point getPos() {
        return pos;
    }
    /**
     * Change the visible state
     * @param vis display-true, don't display-false
     */
    public void setVisible(boolean vis) {
        this.visible = vis;
    }
    /**
     * render image, draw the sandwich in the specified location if needed
     */
    public void draw() {
        if (isVisible()) {
            image.drawFromTopLeft(pos.x, pos.y);
        }
    }
    /**
     * judge whether the player encounters sandwich while the sandwich is exist
     * @param player a object of Player
     * @return if meet return true, else return false
     */
    public boolean isMeet(Player player) {
        if (isVisible()){
            if (player.getPos().distanceTo(pos) < ShadowTreasure.CLOSENESS) {
                return true;
            } else {
                return false;
            }
        }else {
            return false;
        }
    }
}
