import bagel.Image;
import bagel.util.Point;
/**
 *  a stationary entity: zombie
 */
public class Zombie{
    /**
     * the zombie's image displayed in the window
     */
    private Image image;
    /**
     * a Point object representing the position of the zombie
     */
    private Point pos;
    /**
     * constructor
     * @param x abscissa
     * @param y ordinate
     */
    public Zombie(double x, double y){
        this.image = new Image("res/images/zombie.png");
        this.pos = new Point(x,y);
    }
    /**
     * @return the Point object representing the position of the zombie
     */
    public Point getPos() {
        return pos;
    }
    /**
     * render image, draw the zombie in the specified location
     */
    public void draw() {
        image.drawFromTopLeft(pos.x, pos.y);
    }
    /**
     * judge whether the player encounters zombie
     * @param player a object of Player
     * @return if meet return true, else return false
     */
    public boolean isMeet(Player player) {
        if (player.getPos().distanceTo(pos) < ShadowTreasure.CLOSENESS) {
            return true;
        } else {
            return false;
        }
    }
}
