import bagel.DrawOptions;
import bagel.Font;
import bagel.Image;
import bagel.util.Colour;
import bagel.util.Point;
/**
 * a movable entity: player
 */
public class Player{
    /**
     * Distance per move
     */
    private static final double STEPSIZE = 10;
    /**
     * energy level threshold
     */
    private static final int ENERGYTHRE = 3;
    /**
     * set the font style for energy display
     */
    private static final Font FONT = new Font("res/font/DejaVuSans-Bold.ttf", 20);
    private static final DrawOptions OPT = new DrawOptions();
    /**
     * the player's image displayed in the window
     */
    private final Image image;
    /**
     * render position of the player in the window
     */
    private Point pos;
    /**
     * Player's current energy value
     */
    private int energy;

    /**
     * constructor
     * @param x abscissa
     * @param y ordinate
     * @param energy energy value
     */
    public Player(double x, double y, int energy) {
        this.image = new Image("res/images/player.png");
        this.pos = new Point(x,y);
        this.energy = energy;
    }
    /**
     * get the player's postion
     * @return a Point object representing the player's position
     */
    public Point getPos() {
        return this.pos;
    }
    /**
     * get the player's current energy value
     * @return the player's current energy value
     */
    public int getEnergy() {
        return this.energy;
    }
    /**
     * Move one step to the target point
     * @param dest target point
     */
    public void moveTo(Point dest){
        double x = dest.x-this.pos.x;
        double y = dest.y-this.pos.y;
        double length = Math.sqrt(x*x+y*y);//The distance between two points
        //Update coordinate points according to direction and step size
        this.pos = new Point(this.getPos().x+STEPSIZE*x/length,this.getPos().y+STEPSIZE*y/length);
    }
    /**
     * Check if the player meets the Zombie and if so reduce energy by 3 and
     * terminate. Otherwise if the player meets the Sandwich increase the energy
     * an set the Sandwich to invisible.Then make players to the designated direction
     * @param tomb need a shadowtree object to get zombies and sandwiches
     */
    public void update(ShadowTreasure tomb){
        // set direction
        if (this.energy >= ENERGYTHRE){
            moveTo(tomb.getZombie().getPos());// direction to zombie
        } else{
            moveTo(tomb.getSandwich().getPos());// direction to sandwich
        }
        //meet zombies
        if (tomb.getZombie().isMeet(this)) {
            reachZombie();//reduce energy
            tomb.setEndGame(true);//stop game
        }
        //meet sandwich
        else if (tomb.getSandwich().isMeet(this)) {
            eatSandwich();//increase energy
            tomb.getSandwich().setVisible(false);//clear sandwich
        }
    }
    /**
     * draw player
     */
    public void draw() {
        image.drawFromTopLeft(pos.x, pos.y);
        // also show energy level
        FONT.drawString("energy: "+ energy,20,760, OPT.setBlendColour(Colour.BLACK));
    }
    /**
     * Meet a sandwich, increase energy
     */
    public void eatSandwich(){
        energy += 5;
    }
    /**
     * Encounter zombies, reduce energy
     */
    public void reachZombie(){
        energy -= 3;
    }
}
